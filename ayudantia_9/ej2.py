def area_cuadrado():
    x = int(input("Lado del cuadrado en cm: "))
    print(f"El área del cuadrado es {x*x} cm^2")


def area_triangulo():
    base = int(input("Base del triángulo en cm: "))
    altura = int(input("Altura del triángulo en cm: "))
    print(f"El área del triángulo es {(1/2)*base*altura} cm^2")


def info_user():
    nombre = input("Ingrese su nombre: ")
    edad = input("Ingrese su edad: ")
    print(f"Hola {nombre}! Tienes {edad} años de edad")


def adivinar_numero():
    print("Work in progress!")


def display_menu():
    print("======================")
    print("       Menu v0.1      ")
    print("======================")
    print("| 1. Área cuadrado   |")
    print("| 2. Área triangulo  |")
    print("| 3. Nombre y edad   |")
    print("| 4. Adivinar número |")
    print("|                    |")
    print("| 0. SALIR           |")
    print("======================")


n = 1
while (n != 0):
    display_menu()
    n = int(input("Ingrese opción: "))
    if (n == 1):
        area_cuadrado()
    elif (n == 2):
        area_triangulo()
    elif (n == 3):
        info_user()
    elif (n == 4):
        adivinar_numero()
