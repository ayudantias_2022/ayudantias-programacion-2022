---
title: "Programación Ayudantía N.5"
author: Daniel Sánchez
date: May 23, 2022
geometry: margin=2cm
output: pdf_document
---

## Funciones


- Uso
   
   - Las funciones sirven para poder modular nuestro código y hacerlo mucho más legible y utilizable.
   - Si escribiésemos todo nuestro código de manera empírica sería prácticamente imposible de modificar y de entender.
   - Esta manera de separar nuestro código en funciones corresponde al paradigma procedural en la escritura de código.

- Empírico vs Procedural

    Tengamos el ejemplo de preguntar datos al usuario, necesitaremos su nombre y edad, hasta que ingrese 0 y el programa termine.

- Empírico:

    ```python
    end = 1
    while (end != 0):
        nombre = input("Ingrese su nombre: ")
        edad = int(input("Ingrese su edad: "))
        end = int(input("Ingrese 0 para terminar, sino cualquier otro número: "))
    ```

- Procedural:

    ```python
    end = 1

    def calcular_area_cuadrado():
        x = int(input("Ingrese valor del lado, 0 para salir: "))
        print(f"Área de cuadrado: {x*x}")
        return x

    while (end != 0):
        end = calcular_area_cuadrado()

    ```

- Procedural pasando y retornando variables:

    ```python
    def pedir_info(nombre, edad, end):
        nombre = input("Ingrese su nombre: ")
        edad = int(input("Ingrese su edad: "))
        end = int(input("Ingrese 0 para terminar, sino cualquier otro número: "))
        return nombre, edad, end


    nombre = ""
    edad = 1
    end = 1
    while (end != 0):
        nombre, edad, end = pedir_info(nombre, edad, end)
    ```

1. Haga una función que permita mostrar por pantalla un cuadrado con solamente sus bordes, el usuario ingresará el tamaño del cuadrado.

    ```terminal
    Ingrese tamaño: 5
    * * * * *
    *       *
    *       *
    *       *
    * * * * *
    ```

2. Haga una función que muestre por pantalla un menú al usuario con opciones, como calcular el área de diferentes figuras, preguntar y mostrar por pantalla nombre y edad, y/o también jugar al adivina el número.