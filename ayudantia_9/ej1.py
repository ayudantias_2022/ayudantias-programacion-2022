def imprimir_cuadrado(x):
    print("* "*x)
    for i in range(x-2):
        print("* ", end=" ")
        print("  "*(x-3), end=" ")
        print("* ")
    print("* "*x)

imprimir_cuadrado(5)
