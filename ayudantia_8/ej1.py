import numpy as np

arreglo1 = np.array([0.0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
arreglo2 = np.zeros([10])

print("1D:")
print(arreglo1)
print(arreglo2)


arr_2d = np.zeros([5, 5])
print("\n2D:")

for i in range(0, 5):
    for j in range(0, 5):
        arr_2d[i, j] = i

print(arr_2d)
