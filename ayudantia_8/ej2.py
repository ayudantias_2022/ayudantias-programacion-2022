import numpy as np  # Matriz
import random as rd  # Aleatorio

matriz = np.zeros([10, 10])

for i in range(1, 9):
    for j in range(1, 9):
        matriz[i, j] = rd.randint(0, 9)

print(matriz)

print()
contador = 0
for i in range(1, 7):
    for j in range(2, 8):
        if (matriz[i, j] > matriz[i, j-1] and matriz[i, j] > matriz[i, j+1] and
            matriz[i+1, j] > matriz[i+1, j-1] and matriz[i+1, j] > matriz[i+1, j+1] and
                matriz[i+2, j] > matriz[i+2, j-1] and matriz[i+2, j] > matriz[i+2, j+1]):
            contador += 1
            print(
                f"Cordon {contador}: [{i},{j}] [{i+1},{j}] [{i+2},{j}]")

print(f"Esta matriz contiene {contador} cordones montañosos")
