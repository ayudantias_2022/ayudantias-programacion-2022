import math

X_anterior = 1.000
X_actual = 0.0
n = 8

error = True

# while (error == True):
#     X_actual = (2*math.pow(X_anterior, 3) + n)/(3*math.pow(X_anterior, 2))
#     if (abs(X_actual-X_anterior) <= 0.001):
#         error = False
#     X_anterior = X_actual


while (True):
    X_actual = (2*math.pow(X_anterior, 3) + n)/(3*math.pow(X_anterior, 2))
    if (abs(X_actual-X_anterior) <= 0.001):
        break
    else:
        X_anterior = X_actual

print(f"Raiz de {n}: {X_actual}")
