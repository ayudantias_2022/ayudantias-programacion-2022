import random

n = random.randint(3, 13)
chars = ["*", "#", "$", "+"]


if (n % 2 == 1):
    for i in range(0, n+1, 2):
        print(" "*int((n-i)/2), end="")
        print(random.choice(chars)*(i+1))
else:
    for i in range(1, n, 2):
        print(" "*int((n-i)/2), end="")
        print(random.choice(chars)*(i+1))
