---
title: "Programación Ayudantía N.6"
author: Daniel Sánchez
date: May 9, 2022
geometry: margin=2cm
output: pdf_document
---

** Módulos

Básicamente son funciones ya creadas por otra persona/comunidad y que nos servirá
para que nosotros los utilicemos.

**Nota**: Para futuros ramos, como ÁLGEBRA LINEAL, se utiliza mucho el módulo sympy
para trabajar con matrices, así que es de vital importancia que aprendan cómo usar un
módulo, ver su documentación y así aprender de manera autodidacta.

1. Importar

    En este ejemplo importaremos la biblioteca random y la usaremos.

    ```python
    import random
    ```

    **OJO**, si importamos una biblioteca de la siguiente manera:

    ```python
    from random import *
    ```
    No necesitaremos recurrir al random. cuando querramos llamar a una función, sino que solamente escribiremos la función. Tengan cuidado con este uso ya que pueden haber conflictos entre bibliotecas, por lo que no es muy recomendable esta práctica, a menos que trabajen con 1 biblioteca.

2. Verificar sus funciones

    Como preferencia personal les recomiendo ir directamente a la página web para verificar qué funciones trae este módulo y cómo usarlas. Ver este [link](https://docs.python.org/3/library/random.html).

3. Empezar a jugar con sus funciones

    Con
    ```python
    random.randint(a,b)
    ```
    generaremos un entero aleatorio $\in [a,b]$.

    Con
    ```python
    random.random()
    ```
    generaremos un entero aleatorio $\in [0,1]$.

    Con
    ```python
    random.uniform(x,y)
    ```
    generaremos un entero aleatorio $\in [x,y]$.

4. ¿Qué hago si no tengo la biblioteca instalada?

    En tu terminal o consola:
    ```terminal
    pip3 install <nombre_de_la_biblioteca>
    ```
    Sin los <>.

5. Escribe un programa que genere una pirámide con una base aleatoria entre 3 y 13 usando el módulo random.

    ```python
    # Numero generado aleatoriamente: 7

       *
      ***
     *****
    *******
    ```

6. Ahora haz que la piramide tenga sus pisos de carácteres aleatorios dado un arreglo con los carácteres que quieras.

    ```python
    # Numero generado aleatoriamente: 7
    # Arreglo de carácteres: ["*", "#", "$", "+"]
    ```

    ```terminal
        $
       +++
      *****
     #######
    +++++++++
    ```