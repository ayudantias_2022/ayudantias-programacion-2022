import math


def raiz_cubica(num):
    X = 1
    Xant = 0
    while math.fabs((X-Xant)) >= 0.001:
        Xant = X
        X = (2*math.pow(X, 3)+num)/(3*math.pow(X, 2))
    return X
