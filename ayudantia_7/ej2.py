import random
import numpy

tabla = numpy.zeros((10, 10))

for i in range(1, 9):
    for j in range(1, 9):
        tabla[i][j] = random.randint(0, 9)


counter = 0
for i in range(2, 6):
    for j in range(2, 6):
        if (tabla[i][j-1] < tabla[i][j] and tabla[i][j+1] < tabla[i][j] and
            tabla[i+1][j-1] < tabla[i+1][j] and tabla[i+1][j+1] < tabla[i+1][j] and
                tabla[i+2][j-1] < tabla[i+2][j] and tabla[i+2][j+1] < tabla[i+2][j]):
            counter += 1
            print(f"Cordon {counter}: [{i}][{j}] [{i+1}][{j}] [{i+2}][{j}]")

print(f"En total son {counter} cordon(es) montañoso(s)")
