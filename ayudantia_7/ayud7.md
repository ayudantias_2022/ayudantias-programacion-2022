---
title: "Programación Ayudantía N.5"
author: Daniel Sánchez
date: May 23, 2022
geometry: margin=2cm
output: pdf_document
---

## __Masterizar Numpy y Ciclos__


1. 
    Una forma de calcular la raíz cúbica de un número $n$ es iterar con la siguiente fórmula:
    $$ X_{actual} = \frac{2 \cdot X_{anterior}^3 + n}{3 \cdot X_{anterior}^2}$$
    Debemos parar cuando el error entre mi resultado actual y el anterior sea menor a 0.001. Por ejemplo, para calcular la raíz de 8:

        | X = 1.000 ; n = 8                             | Error                          |
        | --------------------------------------------- | ------------------------------ |
        | X = (2 * 1.000^3 + 8) / (3 * 1.000^2) = 3.333 | 3.333 - 1.000 = 2.333 > 0.001  |
        | X = (2 * 3.333^3 + 8) / (3 * 3.333^2) = 2.462 | 3.333 - 2.462 = 0.871 > 0.001  |
        | X = (2 * 2.462^3 + 8) / (3 * 2.462^2) = 2.081 | 2.462 - 2.081 = 0.381 > 0.001  |
        | X = (2 * 2.081^3 + 8) / (3 * 2.081^2) = 2.003 | 2.081 - 2.003 = 0.078 > 0.001  |
        | X = (2 * 2.003^3 + 8) / (3 * 2.003^2) = 2.000 | 2.003 - 2.000 = 0.003 > 0.001  |
        | X = (2 * 2.000^3 + 8) / (3 * 2.000^2) = 2.000 | 2.000 - 2.000 = 0.000 <= 0.001 |
    
    Por lo tanto la raíz de $8$ es $2$.

    Escriba este programa en python.

2. 
   1. Escribe un programa en python que cree, llene y despliegue el contenido de una matriz $10$x$10$. Cada casilla de la matriz debe contener un número aleatorio entre $0$ y $9$, excepto las casillas de la orilla que deben tener el valor $0$ sí o sí.
   Ejemplo:

        <img src="tabla.png">

   2. Al programa anterior, agrégale el código necesario para contar y
   desplegar por pantalla el número de cordones montañosos y la posición de las
   casillas en las que se encuentra cada cordón. Un cordón es un trío de casillas en línea vertical, cada una de ellas con valor mayor a su vecinas izquierda y
   derecha.

        <img src="output.png">
   