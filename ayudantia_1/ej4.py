numero = int(input("Ingresa un numero: "))

if numero % 2 == 0:
    if numero > 20:
        print("El numero es par y es mayor que 20.")
    elif numero < 20:
        print("El numero es par y es menor que 20.")
    else:
        print("El numero es par y es igual a 20.")
else:
    if numero > 20:
        print("El numero es impar y es mayor que 20.")
    elif numero < 20:
        print("El numero es impar y es menor que 20.")
    else:
        print("El numero es impar y es igual a 20.")
