---
title: "Programación Ayudantía N.1"
author: Daniel Sánchez
date: March 28, 2022
geometry: margin=2cm
output: pdf_document
---

## Nivel 1: Condicionales

1. Escriba un programa que pida los siguientes datos:
    - Nombre
    - País de nacimiento
    - Edad

    ```bash
    =====================================
     Bienvenido al comparador de números 
    =====================================
    ¡Hola Daniel! Naciste en Chile y tienes 29 años de edad.
    ```

2. Escriba un programa que pida dos números y que conteste cuál es el menor y cuál es el mayor o que escriba que son iguales.

    ```bash
    =====================================
     Bienvenido al comparador de números 
    =====================================
    El primero número es mayor: 4 > 1
    ```

3. Escriba un programa que pida el año actual y un año cualquiera. Este debe escribir cuántos años faltan para llegar a ese año o bien, cuántos años han pasado.

    ```bash
    ====================================
     Bienvenido al distanciador de años 
    ====================================
    Desde el año 1997, han pasado 17 años
    ```

4. Escriba un programa que determine si un número es par o impar, y si es mayor a 20.

    ```bash
    =========================================
     ¿Tu número es par o impar? ¿Mayor a 20?     
    =========================================
    ¡Tu número es impar y mayor que 20!
    ```

## Nivel 2: ¡Ciclos

1. Imprima los números del 1 al 10 por pantalla usando algún tipo de ciclos, no lo haga manualmente.

    ```bash
    1 2 3 4 5 6 7 8 9 10
    ```
