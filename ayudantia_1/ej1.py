# Datos del usuario
nombre = input("Ingrese su nombre: ")
pais_de_nacimiento = input("Ingrese su país de nacimiento: ")
edad = int(input("Ingrese su edad: "))


## Visualizando problema de las comas.
# print("Hola", nombre, ", cómo estás?")
# print("Hola {}, cómo estás?".format(nombre))
# print()

# Mostrando por pantalla
# Homólogo
print("Hola {}, eres de {} y tienes {} años.".format(nombre, pais_de_nacimiento, edad))
print(f"Hola {nombre}, eres de {pais_de_nacimiento} y tienes {edad} años.")
