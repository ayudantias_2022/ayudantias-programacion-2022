print()

# De la forma for
for i in range(1, 11):  # [0,10[ o [0,9]
    print(i, end=" ")
print()


print()

# De la forma while
i = 1
while i <= 10:
    print(i, end=" ")
    i = i + 1

print()
