user_input = []

condition = True

while (condition):
    user_input.append(float(input("Ingrese un numero: ")))

    continuar = int(input("Para terminar ingrese 0, sino otro numero: "))
    if (continuar == 0):
        condition = False

print(user_input)

# Ordenando de manera ascendente
user_input.sort(reverse=False)

# Mostrando ordenado
print(user_input)

# Ordenando de manera descendente
user_input.sort(reverse=True)

# Mostrando ordenado
print(user_input)
