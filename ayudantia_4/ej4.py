original_dic = {
    "USA": 77,
    "Spain": 63,
    "UK": 15
}

print(original_dic)

# Este método se prefiere
# original_dic.update({"Argentina": 45})
# original_dic.update({"Chile": 20})
# original_dic.update({"Spain": 25})

original_dic.update([("Argentina", 45), ("Chile", 20), ("Spain", 25)])

print(original_dic)
