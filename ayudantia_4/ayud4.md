---
title: "Programación Ayudantía N.4"
author: Daniel Sánchez
date: April 25, 2022
geometry: margin=2cm
output: pdf_document
---

## Ciclos y listas

1. Tipos de estructura de datos:  

    Syntax:
    ```python
    # List
    new_list = [4, 0, "Hola", 3.1415, True, [-2, 1]]

    # Tuple
    new_tuple = ("abc", 34, True, 40, "male")

    # Set
    new_set = {"apple", -3, "street", "coke", 45}

    # Dictionary
    new_dic = {
        "brand" : "Ford",
        "model" : "Mustang",
        "year" : 2014
    }
    ```

    ```
    ---------------------------------------------------------
    | Type of data | Ordered | Changeable | Allow duplicate |
    ---------------------------------------------------------
    | List         |  Yes    |    Yes     |      Yes        |
    | Tuple        |  Yes    |    No      |      Yes        |
    | Set          |  No     |    ---     |      No         |
    | Dictionary   |  ---    |   3.7 Yes  |      No         |
    ---------------------------------------------------------
    ```

2. Escriba un programa que filtre una `lista` de nombres y deje solamente los nombres de tus amigos, sabiendo que el nombre de tus amigos solamente tienen 6 letras.

    Output:
    ```python
    # input: ["Daniel", "Carlos", "Juan", "Gonzalo", "Danilo"]
    Tus amigos son ['Daniel', 'Carlos', 'Danilo']
    ```

    Realice este ejemplo con `Listas`, `Tuplas` y `Sets`. Identifique las diferencias al trabajar con los distintos tipos de estructura de datos.

3. Escriba un programa que determine el tipo de variable de cada variable en una `lista` y que la imprima al revés.

    Output:
    ```python
    # input: [3.2, "Hello", 3, False, [1,2,3]]
    <class 'float'>
    <class 'str'>
    <class 'int'>
    <class 'bool'>
    <class 'list'>
    [[1,2,3], False, 3, "Hello", 3.2]
    ```

4. Escriba un script que agregue los siguientes paises al diccionario y luego los muestre por pantalla:
    
    ```
    Chile: 33
    Argentina: 45
    Spain: 25
    ```

    ```python
    original_dic = {
        "USA" : 77,
        "Spain" : 63,
        "UK" : 15
    }
    ```

    Output (puede ser diferente en orden):
    ```console
    {"USA": 77, "Spain": 33, "UK": 15, "Argentina": 45, "Chile": 33}
    ```

    Hint: Googlea y busca el uso de `.update()` para los diccionarios.

5. Pida al usuario numeros enteros y/o decimales y luego muéstrelos por pantalla de manera ordenada de menor a mayor.

    Output:
    ```python
    # User's input: [65, 33, -2, 4.5, 0, 17]
    [-2, 0, 4.5, 17, 33, 65]
    ```