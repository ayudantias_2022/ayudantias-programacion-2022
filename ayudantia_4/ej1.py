# Lista
new_list = [4, 0, "Hola", 3.1415, True, [-2, 1]]

# Tupla
new_tuple = ("abc", 34, True, 40, "male")

# Set
new_set = {"apple", -3, "street", "coke", 45}

# Dictionary
new_dic = {
    # key : value
    "brand": "Tesla",
    "model": "Mustang",
    "year": 2022,
}

datos = ["Daniel", 1.79, 21]
persona = ["Nombre", "Estatura (m)", "Edad"]

persona_dic = {
    "Nombre": "Daniel",
    "Estatura (m)": 1.79,
    "Edad": 21
}

# print(f"{persona[0]} : {datos[1]}")
# print(persona_dic["Nombre"])

# ###################################
# print(persona_dic.keys())
###################################

# print(new_tuple)
# print(new_tuple[0])

###################################

# print(new_set)
# print(new_set)
# print(new_set)

###################################

print(new_dic)

###################################