#              0      1     2    3        4
user_input = [3.2, "Hello", 3, False, [1, 2, 3]]

# Por elemento
for i in user_input:
    print(type(i))

# Por index
# for i in range(0, len(user_input)):
#     print(type(user_input[i]))

# Imprimir al reves una lista

print(user_input[::-1])


for i in range(len(user_input)-1, -1, -1):
    print(user_input[i], end=" ")

print()
