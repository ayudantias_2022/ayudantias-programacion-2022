n = int(input("Ingrese un numero n: "))
x = int(input("Ingrese un numero x: "))
y = int(input("Ingrese un numero y: "))

if (n % x == 0 and n % y == 0):
    print("Verdadero, porque {} es divisible por {} y {}".format(n, x, y))
elif (n % y != 0 and n % x != 0):
    print("Falso, porque {} no es divisible ni por {} ni por {}".format(n, x, y))
elif (n % x != 0):
    print("Falso, porque {} no es divisible por {}".format(n, x))
elif (n % y != 0):
    print("Falso, porque {} no es divisible por {}".format(n, y))
