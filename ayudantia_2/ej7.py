lista_ingresada = [1, -4, 2, 6]

suma_pos = 0
suma_neg = 0

# Dos maneras de hacer el mismo recorrido

# for i in lista_ingresada:
#     if (i > 0):
#         suma_pos += i
#     else:
#         suma_neg += i

for i in range(len(lista_ingresada)):
    if (lista_ingresada[i] > 0):
        suma_pos += lista_ingresada[i]
    else:
        suma_neg += lista_ingresada[i]

print("La suma de los numeros positivos es: {}".format(suma_pos))
print("La suma de los numeros negativos es: {}".format(suma_neg))
