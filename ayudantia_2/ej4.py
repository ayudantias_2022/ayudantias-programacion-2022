palabras = ["hola", "ayudantia", "ejemplo", "no se"]

elegida = input("Ingrese una palabra: ")

if (elegida in palabras):
    print("La palabra '{}' se encuentra en la lista".format(elegida))
else:
    print("La palabra '{}' no se encuentra en la lista".format(elegida))
