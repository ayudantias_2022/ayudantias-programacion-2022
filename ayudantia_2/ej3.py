# Lista
my_first_list = [3, 22, 10, -4, -1]

print("Suma simple: {}".format(sum(my_first_list)))

suma = 0
for i in my_first_list:
    suma += i

print("Suma complicada: {}".format(suma))
