# Definición de variables
c_x = int(input("Coordenada X del circulo: "))
c_y = int(input("Coordenada Y del circulo: "))
radius = int(input("Radio de la circunferencia: "))
p_x = int(input("Coordenada X del punto: "))
p_y = int(input("Coordenada Y del punto: "))

distance = ((p_x-c_x)**2 + (p_y - c_y)**2)**(1/2)

print()
if (distance > radius):
    print("Se encuentra fuera de la circunferencia")
elif (distance == radius):
    print("Se encuentra sobre el circulo")
else:
    print("Se encuentra dentro del circulo")
