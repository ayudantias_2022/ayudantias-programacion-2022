n = int(input("Ingrese un numero: "))

print("El inverso aditivo {}, es: {}".format(n, n*-1))
print(
    "El inverso multiplicativo {:.1f}, es: 1/{} = {:.3f}".format(n, n, n**-1))
