---
title: "Programación Ayudantía N.2"
author: Daniel Sánchez
date: April 4, 2022
geometry: margin=2cm
output: pdf_document
---

## Nivel 1: Condicionales

1. Escriba un programa que pida:
    - Radio de un círculo.
    - Coordenadas del centro del círculo.
    - Coordenadas de un un punto P cualquiera.

    El programa debe responder si el punto se encuentra adentro o afuera de la circunferencia.  

    Output:

    ```bash
    Coordenada X del círculo: 0  
    Coordenada Y del círculo: 0  
    Radio de la circunferencia: 4  
    Coordenada X del punto: 0  
    Coordenada Y del punto: 2  

    Se encuentra dentro de la circunferencia
    ```

2. Escribe un programa que chequee si un número es divisible por dos números (si o sí), todos los números que se ingresan son positivos y distintos de cero.

    Output:

    ```bash
    Ingresa un numero n: 6  
    Ingresa el numero x: 3  
    Ingresa el numero y: 4  
    Falso, porque 6 no es divisible por 4  
    ```

3. Dada una lista de enteros, determine si la suma de sus números es par o impar.

    Output:

    ```bash
    # input: [2,0,8]  
    La suma de los números es par
    ```

4. Escriba un programa que detecte si una palabra se encuentra en un arreglo de palabras.

    Output:

    ```bash
    # input: ["hola","ayudantia","ejemplo","no se"]  
    Ingrese una palabra: no se  
    La palabra se encuentra en la lista
    ```

5. Haga un programa que devuelva tanto el inverso aditivo como el inverso multiplicativo de un número.

    Output:

    ```bash
    Ingrese un número: 3  
    El inverso aditivo de 3, es: -3  
    El inverso multiplicativo de 3, es: 1/3 = 0.333
    ```

## Nivel 2: Ciclos

1. Dada una lista de números, muestra en pantalla la lista con cada número multiplicado por 2.

    Output:

    ```bash
    # input [3,4,5]  
    La lista multiplicada por 2 es [6, 8, 10]
    ```

2. Dada una lista de números, muestra en pantalla la suma de los números positivos y de los números negativos.

    Ouput:

    ```bash
    # input: [1,-4,2,6]  
    La suma de los números positivos es: 9  
    La suma de los números negativos es: -4
    ```

<!-- 3. Escriba un programa que filtre una lista de nombres y deje solamente los nombres de tus amigos, sabiendo que el nombre de tus amigos solamente tienen 6 letras.

    Output:
    ```python
    # input: ["Daniel", "Carlos", "Juan", "Gonzalo", "Danilo"]
    Tus amigos son ['Daniel', 'Carlos', 'Danilo']
    ``` -->