---
title: "Programación Ayudantía N.3"
author: Daniel Sánchez
date: April 11, 2022
geometry: margin=2cm
output: pdf_document
---

## Ejercicios

1. Escriba un programa que imprima una piramide de '#' con el largo que ingrese el usuario.

    Output:

    ```bash
    Ingrese largo de la base de la piramide: 7

       *
      ***
     *****
    *******
    ```

    ```bash
    Ingrese largo de la base de la piramide: 6

      ##
     ####
    ######
    ```

2. Modifique el programa anterior para imprimir la piramide de manera inversa.

    Output:

    ```bash
    Ingrese largo de la base de la piramide: 7

    *******
     *****
      ***
       *
    ```

    ```bash
    Ingrese largo de la base de la piramide: 6

    ######
     ####
      ##
    ```

3. Escriba un programa que filtre una lista obteniendo los elementos únicos en ella.

    Output:

    ```bash
    # input ="AAAAABBBBCCCDDDEEE"
    ['A','B','C','D','E']

    # input = [1,2,2,2,3,3,4,5,5]
    [1,2,3,4,5]
    ```

4. Escriba un programa que invierta el orden de un numero ingresado por el usuario.

    Ouput:

    ```bash
    Ingrese un numero: 73456

    65437
    ```

5. Escriba un programa que filtre una lista de nombres y deje solamente los nombres de tus amigos, sabiendo que el nombre de tus amigos solamente tienen 6 letras.

    Output:

    ```python
    # input: ["Daniel", "Carlos", "Juan", "Gonzalo", "Danilo"]
    Tus amigos son ['Daniel', 'Carlos', 'Danilo']
    ```

6. Escriba un programa que imprima una piramide de lado.

    Output:

    ```bash
    Ingrese el largo de la base de la piramide: 7

    #
    # #
    # # #
    # # # #
    # # #
    # #
    #
    ```
