---
title: "Programación Ayudantía N.5"
author: Daniel Sánchez
date: May 2, 2022
geometry: margin=2cm
output: pdf_document
---

## Ciclos y listas

1. Tipos de ciclos:  

    Syntax:
    ```python
    # while loop with condition
    i = 0
    while (i < 10):
        print(i)
        i+=1
    
    # while loop without condition
    i = 0
    while (True):
        print(i)
        i+=1
        if (i == 9):
            break

    # for loop
    for i in range(0,10):
        print(i)

    # for loop to access elements on a list
    example_list = [["Leon","Tigre","Puma"],["Gato","Perro","Tortuga"]]

    for i in example_list:
        print(i)
    ```

2. Haga un programa que imprima un cuadrado de caracteres según el tamaño que ingrese el usuario:

    1. 
        Output:
        ```python
        # n = 5
        *****
        *****
        *****
        *****
        *****
        ```
    2. 
        Output:
        ```python
        # n = 5
        * * * * *
        * * * * *
        * * * * *
        * * * * *
        * * * * *
        ```

3. Haga un programa que imprima lo siguiente, dependiendo del input del usuario:

    Output:
    ```python
    # n = 5
    1
    2 2
    3 3 3
    4 4 4 4
    5 5 5 5 5
    ```

4. Haga un programa que imprima una frase ingresada por el usuario sin las vocales:

    Output:
    ```python
    # input = "Hola mundo desde gitlab"
    Hl mnd dsd gtlb
    ```

5. Imprima los valores del diccionario dentro del otro diccionario.

    Ouput:
    ```python
    # nested_dict = {"Dakar" : {"weather" : "sunny", "roads": ["dry","wet"]}}
    ["roads","wet"]
    ```

## Recordando

1. Haga un programa que imprima el siguiente patrón:


    1. Tomando números impares:
        Output:
        ```python
        # n = 5
        *
        * *
        * * *
        * * * *
        * * * * *
        * * * *
        * * *
        * *
        *
        ```

    2. Tomando números pares:
        Output:
        ```python
        # n = 6
        *
        * *
        * * *
        * * * *
        * * * * *
        * * * * * *
        * * * * * *
        * * * * *
        * * * *
        * * *
        * *
        *
        ```