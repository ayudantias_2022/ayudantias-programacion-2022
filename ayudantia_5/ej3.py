frase = input("Frase: ")

vocales = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]

# o
# vocales = "aeiouAEIOU"

# Ejercicio original, quitar las vocales
for i in frase:
    if i not in vocales:
        print(i, end="")

print()

# Quitar los numeros de la frase
for i in frase:
    # Usando metodo .isdecimal()
    if (i.isdecimal() == False):
        print(i, end="")

print()
