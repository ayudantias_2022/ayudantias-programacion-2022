n = int(input("Ingrese un n: "))

# Manera 1
for i in range(n):
    for x in range(i+1):
        print(i+1, end=" ")
    print()

print()

# Manera 2
for i in range(1, n+1):
    for x in range(0, i):
        print(i, end=" ")
    print()
