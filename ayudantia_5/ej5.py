n = int(input("Ingrese n: "))

if (n % 2 != 0):
    for i in range(n):
        for x in range(i+1):
            print("*", end=" ")
        print()

    for i in range(n-1, 0, -1):
        for x in range(i):
            print("*", end=" ")
        print()

# Para numeros pares

if (n % 2 == 0):
    for i in range(n):
        for x in range(i+1):
            print("*", end=" ")
        print()

    for i in range(n, 0, -1):
        for x in range(i):
            print("*", end=" ")
        print()
